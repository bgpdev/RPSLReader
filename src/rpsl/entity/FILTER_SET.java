package rpsl.entity;

import irr.IRR;
import net.ripe.db.whois.common.rpsl.AttributeType;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import net.ripe.db.whois.common.rpsl.RpslObject;
import rpsl.RPSLTokenizer;
import rpsl.expression.filter.Filter;
import rpsl.expression.filter.FilterMatch;
import rpsl.expression.filter.Filterable;

import java.util.Queue;

import static rpsl.expression.filter.FilterMatch.ACCEPT;

public class FILTER_SET
{
    /** The name of the FILTER_SET */
    public final String name;

    /** The IRR snapshot that this FILTER_SET is part of. */
    private final IRR registry;

    /** Filter that is represented by the FILTER_SET. */
    private Filterable filter;

    /**
     * Constructs a FILTER_SET from a name.
     * @param name The name of this FILTER_SET
     * @param registry The IRR database containing potential references.
     */
    public FILTER_SET(String name, IRR registry)
    {
        this.name = name;
        this.registry = registry;
    }

    /**
     * Initializes a FILTER_SET from an RPSLObject.
     * @param object The RpslObject from which the set should be constructed.
     * @throws Exception Any exceptions that occur while constructing the FILTER_SET
     */
    public void construct(RpslObject object) throws Exception
    {
        RpslAttribute r = object.findAttribute(AttributeType.FILTER);
        if(r != null)
        {
            Queue<String> queue = RPSLTokenizer.tokenize(r.getValue());
            filter = Filter.create(queue, registry);
        }
    }

    /**
     * Accepts or rejects a prefix from a certain peer.
     * @param prefix The prefix in question
     * @param peer The peer that forwarded this prefix.
     * @return True if the prefix is accepted by this Filter.
     */
    public FilterMatch accept(String prefix, String peer)
    {
        // If there is no filter present, accept all prefixes.
        if(filter == null)
            return ACCEPT;
        else
            return filter.accept(prefix, peer);
    }
}
