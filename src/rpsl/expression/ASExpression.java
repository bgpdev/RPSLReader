package rpsl.expression;

import irr.IRR;
import network.entities.IP;
import rpsl.RPSLUtility;
import rpsl.entity.AS_SET;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ASExpression
{
    // <as-expression> is an expression over AS numbers and AS sets using operators AND, OR, and EXCEPT,
    // TODO: Implement AND, OR and EXCEPT
    public static boolean match(List<String> tokens, Long ASN, IRR registry)
    {
        return match(tokens, ASN, registry, new HashSet<>());
    }

    public static boolean match(List<String> tokens, Long ASN, IRR registry, Set<String> path)
    {
        for(String token : tokens)
        {
            token = token.toUpperCase().trim();
            switch(token)
            {
                case "AND":
                    System.out.println("[AS-Expression] Unimplemented: AND");
                    break;
                case "OR":
                    System.out.println("[AS-Expression] Unimplemented: OR");
                    break;
                case "EXCEPT":
                    System.out.println("[AS-Expression] Unimplemented: EXCEPT");
                    break;
                default:

                    // Match a literal AS number
                    if(token.matches("AS[0-9]+"))
                    {
                        if (token.equals("AS" + ASN))
                            return true;
                    }

                    // Match the predefined AS_SET 'AS-ANY'
                    else if(token.equals("AS-ANY"))
                        return true;

                        // Match a custom AS_SET 'AS-<name>'
                    else if(RPSLUtility.isAS_SET(token) && !path.contains(token))
                    {
                        AS_SET set = registry.getAS_SET(token);
                        if(set != null && set.accept(ASN))
                            return true;
                    }

                    // TODO: This should be done better.
                    else if(!IP.isValid(token) &&  !token.isEmpty())
                    {
                        System.out.println("Token list:" + tokens);
                        System.out.println("[AS-Expression] Unimplemented Token: " + token);
                    }
                    break;
            }
        }

        return false;
    }
}
