package rpsl.entity;

import irr.IRR;
import net.ripe.db.whois.common.rpsl.AttributeType;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import net.ripe.db.whois.common.rpsl.RpslObject;
import network.entities.Prefix;
import rpsl.RPSLEntity;
import rpsl.RPSLUtility;
import rpsl.expression.Expression;
import rpsl.expression.RangeOperator;
import rpsl.expression.filter.FilterMatch;

import java.util.HashSet;
import java.util.Set;

import static rpsl.expression.filter.FilterMatch.REJECT;

/**
 * A ROUTE_SET object represents an RPSL route-set (set prefixed by rs-) object.
 * Implements the RouteResolvable(IPAddress) interface.
 */
public class ROUTE_SET extends RPSLEntity
{
    /** The name of the ROUTE_SET */
    public final String name;

    /** The IRR snapshot that this ROUTE_SET is part of. */
    private final IRR registry;

    /** The set of direct prefixes and references to ROUTE-SETS. */
    private final Set<String> prefixes = new HashSet<>();
    private final Set<RangeOperator> operators = new HashSet<>();
    private final Set<ROUTE_SET> route_sets = new HashSet<>();
    private final Set<AS_SET> as_sets = new HashSet<>();

    /**
     * Constructs a ROUTE_SET from an RPSLObject.
     * @param name The name of this ROUTE_SET
     * @param registry The IRR database containing potential references.
     */
    public ROUTE_SET(String name, IRR registry)
    {
        this.name = name;
        this.registry = registry;
    }

    public void construct(RpslObject object)
    {
        /*
         * Check for mbrs-by-ref
         * TODO: Check if they are accepted by the SET.
         */

        /*
         * Check the members of the set.
         * TODO: Check if they are accepted by the SET.
         */
        for(RpslAttribute r : object.findAttributes(AttributeType.MEMBERS))
            for(String token : r.getTokenList().get(0).getValue())
            {
                token = token.toUpperCase();

                if (Prefix.isPrefix(token))
                    prefixes.add(token);
                else if (RPSLUtility.isROUTE_SET(token))
                    route_sets.add(registry.getROUTE_SET(token));
                else if (RPSLUtility.isAS_SET(token))
                    as_sets.add(registry.getAS_SET(token));
                else if(RPSLUtility.isAS(token))
                    prefixes.addAll(Expression.getPrefixes(token, registry));
                else if(token.contains("^"))
                    operators.add(new RangeOperator(token, registry));
                else if(!token.isEmpty())
                    System.out.println("[ROUTE_SET]: Unrecogized Token: " + token);
            }
    }

    /**
     * Returns true if the prefix is inside the set represented by the object.
     * @param prefix The prefix that should be matched against this object.
     * @return True if it is inside the set, false otherwise.
     */
    public FilterMatch accept(String prefix)
    {
        return accept(prefix, new HashSet<>());
    }


    public FilterMatch accept(String prefix, Set<String> path)
    {
        // Add this AS_SET name to the path list to prevent recursion.
        path.add(name);

        // Check for direct children
        if(prefixes.contains(prefix))
            return FilterMatch.EXACT_PREFIX_ACCEPT;

        for(RangeOperator operator : operators)
        {
            FilterMatch match = operator.accept(prefix, path);
            if(!match.equals(REJECT))
                return match;
        }

        // Check for children route_sets
        for(ROUTE_SET set : route_sets)
            if (!path.contains(set.name))
            {
                FilterMatch match = set.accept(prefix, path);
                if(!match.equals(REJECT))
                    return match;
            }

        // Check for children AS_SETs
        for(AS_SET set : as_sets)
            if (!path.contains(set.name))
            {
                FilterMatch match = set.accept(prefix);
                if(!match.equals(REJECT))
                    return match;
            }

        return REJECT;
    }

    public FilterMatch accept(String prefix, Set<String> path, String operator)
    {
        // Add this AS_SET name to the path list to prevent recursion.
        path.add(name);

        // Check for direct children
        for(String x : prefixes)
        {
            FilterMatch match = RPSLUtility.matches(prefix, x.split("\\^")[0] + "^" + operator);
            if(!match.equals(REJECT))
                return match;
        }

        // Check for children route_sets
        for(ROUTE_SET set : route_sets)
            if (!path.contains(set.name))
            {
                FilterMatch match = set.accept(prefix, path);
                if(!match.equals(REJECT))
                    return match;
            }

        // Check for children AS_SETs
        for(AS_SET set : as_sets)
            if (!path.contains(set.name))
            {
                FilterMatch match = set.accept(prefix);
                if(!match.equals(FilterMatch.REJECT))
                    return match;
            }

        return REJECT;
    }

    public FilterMatch accept(String prefix, String operator)
    {
        return accept(prefix, new HashSet<>(), operator);
    }
}
