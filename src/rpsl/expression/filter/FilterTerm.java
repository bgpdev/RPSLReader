package rpsl.expression.filter;

import irr.IRR;

import java.util.Queue;

/**
 * Represents a Filter Expression
 * Expression: filter_factor OP_AND filter_term | filter_factor
 */
public class FilterTerm implements Filterable
{
    private final Filterable left;
    private final Filterable right;

    private FilterTerm(Filterable left, Filterable right)
    {
        this.left = left;
        this.right = right;
    }

    /**
     *
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return Returns true if the prefix is accepted by the policy.
     */
    @Override
    public FilterMatch accept(String prefix, String peer)
    {
        if(!left.accept(prefix, peer).equals(FilterMatch.REJECT) && !right.accept(prefix, peer).equals(FilterMatch.REJECT))
            return FilterMatch.ACCEPT;
        else
            return FilterMatch.REJECT;
    }

    public static Filterable create(Queue<String> queue, IRR registry) throws Exception
    {
        Filterable left = FilterFactor.create(queue, registry);

        if(!queue.isEmpty() && queue.peek().equals("AND"))
        {
            queue.remove();
            Filterable right = FilterTerm.create(queue, registry);
            return new FilterTerm(left, right);
        }
        else
            return left;
    }
}
