package rpsl.interfaces;

/**
 * Classes that extend from this class represent objects that a prefix or ASN can be matched against.
 * That is: If it is contained in the set represented by the object.
 */
public interface RouteResolvable
{
    /**
     * Returns true if the prefix is inside the set represented by the object.
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return True if it is inside the set, false otherwise.
     */
    boolean accept(String prefix);
}
