package rpsl.expression.filter;

import irr.IRR;

import java.util.Queue;

import static rpsl.expression.filter.FilterMatch.ACCEPT;

/**
 * Expression: filter_operand: KEYW_ANY | TKN_FLTRNAME | filter_aspath | filter_rp_attribute | filter_prefix
 * TODO:
 * - Implement AS-PATH
 * - Implement RP-attributes
 * - Implement filternames
 */
public class FilterOperand implements Filterable
{
    private final String filter;
    private final IRR registry;

    private FilterOperand(String filter, IRR registry)
    {
        this.filter = filter;
        this.registry = registry;
    }

    public static class PathOperatorException extends Exception
    {
       PathOperatorException(String message)
        {
            super(message);
        }
    }

    /**
     *
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return Returns true if the prefix is accepted by the policy.
     */
    @Override
    public FilterMatch accept(String prefix, String peer)
    {
        if(filter.equals("ANY"))
            return ACCEPT;
        else
            return registry.getFILTER_SET(filter).accept(prefix, peer);
    }

    public static Filterable create(Queue<String> queue, IRR registry) throws Exception
    {
        switch(queue.peek())
        {
            case "ANY":
            {
                return new FilterOperand(queue.remove(), registry);
            }
            case "<":
            {
                throw new PathOperatorException("AS Path operators not yet implemented.");
            }
            default:
            {
                if(queue.peek().startsWith("FLTR-"))
                    return new FilterOperand(queue.remove(), registry);

                return FilterPrefix.create(queue, registry);
            }
        }
    }
}
