package rpsl.expression.filter;

public interface Filterable
{
    /**
     * Returns true if the prefix is inside the set represented by the object.
     * @param prefix The prefix or ASN that should be matched against this object.
     * @param peer The peer AS number from which the prefix was received.
     * @return True if it is inside the set, false otherwise.
     */
    FilterMatch accept(String prefix, String peer);
}
