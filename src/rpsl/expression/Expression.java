package rpsl.expression;

import collections.trees.Node;
import inet.ipaddr.AddressStringException;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import irr.IRR;
import rpsl.entity.AS_SET;
import rpsl.entity.ROUTE;
import rpsl.entity.ROUTE_SET;
import rpsl.expression.filter.FilterMatch;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

public class Expression
{
    public static boolean isOriginatedBy(Long ASN, String prefix, IRR registry)
    {
        Set<rpsl.entity.ROUTE> routes = registry.getRoutes(ASN);
        for(rpsl.entity.ROUTE route: routes)
        {
            if(route.prefix.equals(prefix))
                return true;
        }

        return false;
    }

    public static boolean inAS_SET(String set, Set<String> origins, Set<String> path, IRR registry)
    {
        AS_SET routes = registry.getAS_SET(set);
        return routes != null && !routes.accept(origins, path).equals(FilterMatch.REJECT);
    }

    public static boolean inROUTE_SET(String prefix, String set, Set<String> path, IRR registry)
    {
        ROUTE_SET routes = registry.getROUTE_SET(set);
        return routes != null && !routes.accept(prefix, path).equals(FilterMatch.REJECT);
    }

    public static Set<String> getOrigins(String prefix, IRR registry)
    {
        try
        {
            Set<String> origins = new HashSet<>();
            IPAddress address = new IPAddressString(prefix).toAddress();
            BitSet set = BitSet.valueOf(address.getBytes());
            Node<Set<String>> route = registry.routes.get(set, address.getPrefixLength());
            while(route != null)
            {
                if(route.value != null)
                    origins.addAll(route.value);
                route = route.parent;
            }

            return origins;
        }
        catch (AddressStringException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static Set<String> getPrefixes(String ASN, IRR registry)
    {
        Set<String> prefixes = new HashSet<>();
        for(ROUTE route : registry.getRoutes(Long.parseLong(ASN.replace("AS", ""))))
            prefixes.add(route.prefix);
        return prefixes;
    }
}
