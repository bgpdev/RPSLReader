package rpsl.expression.filter;

import irr.IRR;
import network.entities.Prefix;
import rpsl.RPSLUtility;
import rpsl.expression.Expression;

import java.util.Queue;

/**
 * Expression: filter_prefix_operand: TKN_ASNO | KEYW_PEERAS | TKN_ASNAME | TKN_RSNAME | '{' opt_filter_prefix_list '}'
 */
public class FilterPrefixOperand implements RangeOperatable
{
    private final String operand;
    private final IRR registry;

    public static class InvalidTokenException extends Exception
    {
        InvalidTokenException(String message)
        {
            super(message);
        }
    }

    private FilterPrefixOperand(String operand, IRR registry)
    {
        this.operand = operand;
        this.registry = registry;
    }

    /**
     *
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return True if the prefix is accepted by the policy.
     */
    @Override
    public FilterMatch accept(String prefix, String peer)
    {
        if(RPSLUtility.isAS(operand))
        {
            if (Expression.getOrigins(prefix, registry).contains(operand))
                return FilterMatch.EXACT_PREFIX_ACCEPT;
            else
                return FilterMatch.REJECT;
        }

        if(RPSLUtility.isAS_SET(operand))
            return registry.getAS_SET(operand).accept(prefix);

        if(RPSLUtility.isROUTE_SET(operand))
            return registry.getROUTE_SET(operand).accept(prefix);

        if(operand.equals("PEERAS"))
        {
            if (Expression.getOrigins(prefix, registry).contains(peer))
                return FilterMatch.EXACT_PREFIX_ACCEPT;
            else
                return FilterMatch.REJECT;
        }


        return RPSLUtility.matches(prefix, operand);
    }

    @Override
    public FilterMatch accept(String prefix, String peer, String operator)
    {
        if(RPSLUtility.isAS(operand))
            for(String route : Expression.getPrefixes(operand, registry))
            {
                FilterMatch match = RPSLUtility.matches(route, operator);
                if(!match.equals(FilterMatch.REJECT))
                    return match;
            }

        if(RPSLUtility.isAS_SET(operand))
            return registry.getAS_SET(operand).accept(prefix, operator);

        if(RPSLUtility.isROUTE_SET(operand))
            return registry.getROUTE_SET(operand).accept(prefix, operator);

        if(operand.equals("PEERAS"))
            for(String route : Expression.getPrefixes(peer, registry))
            {
                FilterMatch match = RPSLUtility.matches(route, operator);
                if(!match.equals(FilterMatch.REJECT))
                    return match;
            }

        return RPSLUtility.matches(prefix, operand + "^" + operator);
    }

    public static RangeOperatable create(Queue<String> queue, IRR registry) throws Exception
    {
        if(RPSLUtility.isAS(queue.peek()))
            return new FilterPrefixOperand(queue.remove(), registry);
        if(RPSLUtility.isAS_SET(queue.peek()))
            return new FilterPrefixOperand(queue.remove(), registry);
        if(RPSLUtility.isROUTE_SET(queue.peek()))
            return new FilterPrefixOperand(queue.remove(), registry);
        if(queue.peek().equals("PEERAS"))
            return new FilterPrefixOperand(queue.remove(), registry);
        if(queue.peek().equals("{"))
            return FilterPrefixList.create(queue);

        // Officially this is not supported, but many people provide one prefix without brackets.
        if(Prefix.isPrefix(queue.peek().split("\\^")[0]))
            return new FilterPrefixOperand(queue.remove(), registry);

        throw new InvalidTokenException("NO MATCHING TOKEN: " + queue.peek());
    }
}
