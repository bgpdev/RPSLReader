package rpsl.expression.filter;

import irr.IRR;

import java.util.Queue;

public class FilterPrefix implements Filterable
{
    private final RangeOperatable operand;
    private final String operator;

    private FilterPrefix(RangeOperatable operand, String operator)
    {
        this.operand = operand;
        this.operator = operator;
    }

    /**
     *
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return True if the prefix is accepted by the policy.
     */
    @Override
    public FilterMatch accept(String prefix, String peer)
    {
        return operand.accept(prefix, peer, operator.replace("^", ""));
    }

    public static Filterable create(Queue<String> queue, IRR registry) throws Exception
    {
        RangeOperatable filterable = FilterPrefixOperand.create(queue, registry);
        if(!queue.isEmpty() && queue.peek().startsWith("^"))
            return new FilterPrefix(filterable, queue.remove() + queue.remove());
        else
            return filterable;
    }
}
