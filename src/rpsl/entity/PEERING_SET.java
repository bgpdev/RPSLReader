package rpsl.entity;

import net.ripe.db.whois.common.rpsl.AttributeType;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import net.ripe.db.whois.common.rpsl.RpslObject;

import java.util.HashSet;
import java.util.Set;

public class PEERING_SET
{
    /** The List of ASN numbers that belong to this set **/
    private Set<String> set = new HashSet<>();

    /** The RpslObject of this object. */
    private RpslObject object;

    public PEERING_SET(RpslObject object)
    {
        this.object = object;

        for(RpslAttribute x : object.findAttributes(AttributeType.MEMBERS))
            for(String e : x.getTokenList().get(0).getValue())
                set.add(e.toUpperCase());
    }
}
