package rpsl.expression.filter;

import irr.IRR;

import java.util.Queue;

/**
 * Represents a Filter Expression
 * Expression: filter-term OR filter | filter-term filter-term | filter-term
 */
public class Filter implements Filterable
{
    private final Filterable left;
    private final Filterable right;

    private Filter(Filterable left, Filterable right)
    {
        this.left = left;
        this.right = right;
    }

    @Override
    public FilterMatch accept(String prefix , String peer)
    {
        FilterMatch match = left.accept(prefix, peer);
        if(!match.equals(FilterMatch.REJECT))
            return match;
        else
            return right.accept(prefix, peer);
    }

    public static Filterable create(Queue<String> queue, IRR registry) throws Exception
    {
        if(queue.size() == 0)
            throw new Exception("Empty <filter>");

        Filterable left = FilterTerm.create(queue, registry);

        if(queue.isEmpty() || queue.peek().equals(")") || queue.peek().equals(";"))
            return left;
        else if(queue.peek().equals("OR"))
        {
            queue.remove();
            Filterable right = Filter.create(queue, registry);
            return new Filter(left, right);
        }
        else
        {
            Filterable right = Filter.create(queue, registry);
            return new Filter(left, right);
        }
    }
}
