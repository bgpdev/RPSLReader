package rpsl;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;

/**
 * An RPSLObject represents a record being formatted in the Routing RPSLPolicy Specification Language.
 */
public class RPSLObject
{
    /** The internal representation of the RPSLObject. */
    private JsonObject object;


    /**
     * Construct an RPSLObject from a JSONObject.
     */
    public RPSLObject(String json)
    {
        JsonReader reader = new JsonReader(new StringReader(json));
        reader.setLenient(true);
        this.object = (JsonObject) new JsonParser().parse(reader);
    }

    /**
     * Construct an empty RPSLObject.
     */
    public RPSLObject()
    {
        object = new JsonObject();
    }

    /**
     * Add a property to the RPSLObject
     * @param property The name of the property.
     * @param value The value of the property.
     */
    public void addProperty(String property, String value)
    {
        object.addProperty(property, value);
    }

    /**
     * Retrieves a property from the RPSLObject.
     * @param property The name of the property.
     * @return The value of the property or null if it is not present.
     */
    public String getProperty(String property)
    {
        if(object.has(property))
            return object.get(property).getAsString();
        else
            return null;
    }

    /**
     * Checks if a property is present.
     * @param property The name of the property.
     * @return A boolean indicating if a property is present or not.
     */
    public boolean hasProperty(String property)
    {
        return object.has(property);
    }

    /**
     * Converts the RPSLObject to a JSON format for easy storage and use in networking.
     * @return A JsonObject representing the RPSLObject.
     */
    public JsonObject toJson()
    {
        return object;
    }

    /**
     * Retrieves the amount of properties in the RPSLObject.
     * @return The amount of properties present in the RPSLObject.
     */
    public int size()
    {
        return object.entrySet().size();
    }
}
