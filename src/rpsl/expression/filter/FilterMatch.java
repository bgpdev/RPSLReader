package rpsl.expression.filter;

public enum FilterMatch
{
    ACCEPT,
    EXACT_PREFIX_ACCEPT,
    SPECIFIC_PREFIX_ACCEPT,

    REJECT,

    POLICY_BUT_NO_FILTERING
}
