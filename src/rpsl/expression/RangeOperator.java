package rpsl.expression;

import irr.IRR;
import network.entities.Prefix;
import rpsl.RPSLUtility;
import rpsl.expression.filter.FilterMatch;

import java.util.Set;

public class RangeOperator
{
    private final String operand;
    private final String operator;
    private final IRR registry;

    public RangeOperator(String token, IRR registry)
    {
        // Parse operator
        String[] split = token.split("\\^");
        this.operand = split[0];
        this.operator = split[1];
        this.registry = registry;
    }

    public FilterMatch accept(String prefix, Set<String> path)
    {
        if (Prefix.isPrefix(operand))
            return RPSLUtility.matches(prefix, operand + "^" + operator);

        if (RPSLUtility.isROUTE_SET(operand))
            return registry.getROUTE_SET(operand).accept(prefix, path, operator);

        if (RPSLUtility.isAS_SET(operand))
            return registry.getAS_SET(operand).accept(prefix, operator);

        if(RPSLUtility.isAS(operand))
            for(String x : Expression.getOrigins(prefix, registry))
            {
                FilterMatch match = RPSLUtility.matches(prefix, x + "^" + operator);
                if (!match.equals(FilterMatch.REJECT))
                    return match;
            }

        return FilterMatch.REJECT;
    }
}
