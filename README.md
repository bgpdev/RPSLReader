# README

## Introduction
The RPSLReader project contains a RPSLReader class written in Java that can read RPSL formatted objects from an InputStream.
RPSL stands for Routing Policy Specification Language and it is widely used in WHOIS services.

## Example usage:
To create a RPSLReader just pass in the InputStream:
```
// Create an RPSLReader that reads RPSLObjects from an InputStream.
InputStream stream = ...
RPSLReader reader = new RPSLReader(stream);
```

To read the next RPSLObject from the stream:
```
RPSLObject object = reader.read();
```

To read all the RPSLObjects in the stream:
```
ArrayList<RPSLObject> objects = reader.readAll();
```

# Extra Notes:

- RPSL is case insensitive, so AS1 is the same as as1.