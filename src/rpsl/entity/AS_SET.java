package rpsl.entity;

import irr.IRR;
import net.ripe.db.whois.common.rpsl.AttributeType;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import net.ripe.db.whois.common.rpsl.RpslObject;
import rpsl.RPSLEntity;
import rpsl.RPSLUtility;
import rpsl.expression.Expression;
import rpsl.expression.filter.FilterMatch;
import rpsl.interfaces.ASNResolvable;

import java.util.HashSet;
import java.util.Set;

public class AS_SET extends RPSLEntity implements ASNResolvable
{
    /** The name of the AS_SET */
    public final String name;

    /** The IRR snapshot that this AS_SET is part of. */
    private final IRR registry;

    /** The set of direct ASx numbers and references AS-NAME AS-SETS. */
    private final Set<String> ASx = new HashSet<>();
    private final Set<AS_SET> children = new HashSet<>();

    /**
     * Constructs a AS_SET from an RPSLObject.
     * @param name The name of this AS_SET
     * @param registry The IRR database containing potential references.
     */
    public AS_SET(String name, IRR registry)
    {
        this.name = name;
        this.registry = registry;
    }

    public void construct(RpslObject object)
    {
        /*
         * Check for mbrs-by-ref
         * TODO: Check if they are accepted by the SET.
         */

        /*
         * Check the members of the set.
         * TODO: Check if they are accepted by the SET.
         */
        for(RpslAttribute r : object.findAttributes(AttributeType.MEMBERS))
            for(String token : r.getTokenList().get(0).getValue())
            {
                token = token.toUpperCase();
                if (RPSLUtility.isAS(token))
                    ASx.add(token);
                else if (RPSLUtility.isAS_SET(token))
                    children.add(registry.getAS_SET(token));
                else
                    System.out.println("[AS_SET]: Unrecogized Token: " + token);
            }
    }

    /**
     * Returns true if the prefix is inside the set represented by the object.
     * @param prefix The prefix that should be matched against this object.
     * @return True if it is inside the set, false otherwise.
     */

    public FilterMatch accept(String prefix)
    {
        Set<String> origins = Expression.getOrigins(prefix, registry);
        return accept(origins, new HashSet<>());
    }

    public FilterMatch accept(Set<String> origins, Set<String> path)
    {
        // Add this AS_SET name to the path list to prevent recursion.
        path.add(name);

        // Check for direct children.
        for(String origin : origins)
            if(ASx.contains(origin))
                return FilterMatch.ACCEPT;

        // Check for children in child AS_SETs
        for(AS_SET set : children)
            if (!path.contains(set.name))
            {
                FilterMatch match = set.accept(origins, path);
                if(!match.equals(FilterMatch.REJECT))
                    return match;
            }

        return FilterMatch.REJECT;
    }


    /* -------------------------------------
     * Range Operator - Accept.
     * -------------------------------------*/
    public FilterMatch accept(String prefix, String operator)
    {
        return accept(prefix, operator, new HashSet<>());
    }

    public FilterMatch accept(String prefix, String operator, Set<String> path)
    {
        path.add(name);

        // Check for direct children.
        for(String AS : ASx)
            for(String p : Expression.getPrefixes(AS, registry))
            {
                FilterMatch match = RPSLUtility.matches(prefix, p + "^" + operator);
                if(!match.equals(FilterMatch.REJECT))
                    return match;
            }


        // Check for children in child AS_SETs
        for(AS_SET set : children)
            if (!path.contains(set.name))
            {
                FilterMatch match = set.accept(prefix, operator, path);
                if(!match.equals(FilterMatch.REJECT))
                    return match;
            }

        return FilterMatch.REJECT;
    }

    @Override
    public boolean accept(Long ASN)
    {
        return accept(ASN, new HashSet<>());
    }

    public boolean accept(Long ASN, Set<String> path)
    {
        path.add(name);

        if(ASx.contains("AS" + ASN))
            return true;

        for(AS_SET set : children)
            if(!path.contains(set.name) && set.accept(ASN, path))
                return true;

        return false;
    }
}
