package rpsl;

import irr.IRR;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import rpsl.expression.ASExpression;
import rpsl.expression.filter.Filter;
import rpsl.expression.filter.FilterMatch;
import rpsl.expression.filter.Filterable;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Each RPSLPolicy represents a policy that can accept or reject a prefix originated by a peer.
 */
public class RPSLPolicy implements Filterable
{
    /** The ASN number of the AS that owns this policy. */
    public final Long asn;

    /** The <as-expression> representing the group of peers that this policy applies to. */
    private List<String> peers = new ArrayList<>();

    /** The Filter that we can match against. */
    private Filterable filter;

    /** The RPSL object that represents the policy */
    private final RpslAttribute attribute;

    /** The IRR snapshot that this registry belongs to */
    private IRR registry;

    /**
     * Constructs an RPSLPolicy
     * @param asn The ASN of the AS that owns this policy.
     * @param attribute The RPSL attribute that represents the policy.
     * @param registry The IRR snapshot that this policy belongs to.
     * @throws Exception Any exceptions that might be thrown during parsing.
     */
    public RPSLPolicy(Long asn, RpslAttribute attribute, IRR registry) throws Exception
    {
        this.asn = asn;
        this.registry = registry;
        this.attribute = attribute;

        // Tokenize the import / export attribute.
        Queue<String> queue = RPSLTokenizer.tokenize(attribute.getValue());

        while(!queue.isEmpty())
        {
            switch (queue.remove())
            {
                case "FROM":
                case "TO":
                {
                    peers.add(queue.remove());
                    break;
                }
                case "ANNOUNCE":
                {
                    filter = Filter.create(queue, registry);
                    break;
                }
                case "ACCEPT":
                {
                    filter = Filter.create(queue, registry);
                    break;
                }
            }
        }
    }

    public FilterMatch accept(String prefix, String peer)
    {
        // If there is no filter present, accept all prefixes.
        if(filter == null)
            return FilterMatch.POLICY_BUT_NO_FILTERING;
        else
            return filter.accept(prefix, peer);
    }

    /**
     * Checks if this RPSLPolicy matches the peer provided.
     * @param ASN The ASN to which this RPSLPolicy is matched against.
     * @return True if this RPSLPolicy applies to this ASN, false otherwise.
     * Expression: as-expression [router-expression-1] [at router-expression-2] | peering-set-name
     *
     * TODO: Parse the structure accordingly
     * TODO: Parse mp-import and mp-export formats.
     */
    public boolean hasPeer(long ASN)
    {
        return ASExpression.match(peers, ASN, registry);
    }

    /**
     * Retrieves raw RPSL Policy from which the Filter is contructed.
     * @return The raw RPSL Policy as has been specified in the IRR.
     */
    public String getPolicy()
    {
        return attribute.getCleanValue().toString();
    }
}
