package rpsl.interfaces;

public interface ASNResolvable
{
    /**
     * Returns true if the prefix is inside the set represented by the object.
     * @param ASN The prefix or ASN that should be matched against this object.
     * @return True if it is inside the set, false otherwise.
     */
    boolean accept(Long ASN);
}
