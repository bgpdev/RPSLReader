package rpsl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class RPSLTokenizer
{
    public static Queue<String> tokenize(String tokens)
    {
        LinkedList list = new LinkedList();

        String[] split = tokens.split("#")[0].split("\\s+");
        for(String x : split)
        {
            StringTokenizer tokenizer = new StringTokenizer(x, "<>(){}[],=;^", true);
            while(tokenizer.hasMoreTokens())
                list.push(tokenizer.nextToken().trim().toUpperCase());
        }

        Collections.reverse(list);

        return list;
    }
}
