package irr;

import collections.trees.BinaryTree;
import collections.trees.Node;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import net.ripe.db.whois.common.domain.CIString;
import net.ripe.db.whois.common.io.RpslObjectStreamReader;
import net.ripe.db.whois.common.rpsl.AttributeType;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import net.ripe.db.whois.common.rpsl.RpslObject;
import rpsl.RPSLPolicy;
import rpsl.entity.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

/**
 * The IRR object holds the global status of the Internet Routing Registries.
 */
public class IRR
{
    /** The Map of AS_SETs */
    public final Map<String, AS_SET> as_set = new HashMap<>();

    /** The Map of the PEERING_SETs. */
    public final Map<String, PEERING_SET> peering_set = new HashMap<>();

    /** The Map of the FILTER_SETs. */
    public final Map<String, FILTER_SET> filter_set = new HashMap<>();

    /** The Map of the ROUTE_SETs. */
    public final Map<String, ROUTE_SET> route_set = new HashMap<>();

    /** The Map of the AUTNUMs. */
    public final Map<Long, AUTNUM> autnums = new HashMap<>();

    /** The Map for Prefix to Set(ASN). */
    public final BinaryTree<Set<String>> routes = new BinaryTree<>();

    /** Private mappings for internal lookup */
    private final Map<Long, Set<ROUTE>> asn_route = new HashMap<>();
    private final Map<String, Set<ROUTE>> member_route = new HashMap<>();

    private final Map<Long, Set<RPSLPolicy>> import_policies = new HashMap<>();
    private final Map<Long, Set<RPSLPolicy>> export_policies = new HashMap<>();

    /**
     * Constructs the IRR that holds the global state of all registries.
     * @param time The time for which the IRR snapshot should be created.
     */
    public IRR(Instant time)
    {
        try
        {
            System.out.println("IRR: Parsing the RPSL files!");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC);
            try (Stream<Path> paths = Files.walk(Paths.get("archives/" + formatter.format(time) + "/irr/raw/")))
            {
                paths.filter(Files::isRegularFile).forEach((Path path) ->
                {
                    try
                    {
                        System.out.println("[IRR] Parsing stream " + path.getFileName());
                        parse(new GZIPInputStream(new FileInputStream(path.toFile())));
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                });
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }

        System.out.println("IRR: Done!");
    }

    private void parse(InputStream stream)
    {
        /* ------------------------------------------------------
         * Parse the RPSLObjects.
         * ------------------------------------------------------*/
        for(String x : new RpslObjectStreamReader(stream))
        {
            try
            {
                RpslObject object = RpslObject.parse(x);
                switch (object.getType())
                {
                    case AS_SET:
                    {
                        String name = object.findAttribute(AttributeType.AS_SET).getValue().trim().toUpperCase();

                        AS_SET set = as_set.getOrDefault(name, new AS_SET(name, this));
                        set.construct(object);

                        if (!as_set.containsKey(name))
                            as_set.put(name, set);

                        break;
                    }
                    case FILTER_SET:
                    {
                        String name = object.findAttribute(AttributeType.FILTER_SET).getValue().trim().toUpperCase();

                        FILTER_SET set = filter_set.getOrDefault(name, new FILTER_SET(name, this));
                        set.construct(object);

                        if (!filter_set.containsKey(name))
                            filter_set.put(name, set);

                        break;
                    }
                    case PEERING_SET:
                        // peering_set.put(object.getKey().toString().toUpperCase(), new PEERING_SET(this, object));
                        break;
                    case ROUTE_SET:
                    {
                        String name = object.findAttribute(AttributeType.ROUTE_SET).getValue().trim().toUpperCase();

                        ROUTE_SET set = route_set.getOrDefault(name, new ROUTE_SET(name, this));
                        set.construct(object);

                        if (!route_set.containsKey(name))
                            route_set.put(name, set);

                        break;
                    }
                    case AUT_NUM:
                    {
                        Long ASN = Long.parseLong(object.findAttribute(AttributeType.AUT_NUM).getCleanValue().toString().toUpperCase().replaceAll("AS", ""));
                        AUTNUM autnum = new AUTNUM(object, this);

                        if (!import_policies.containsKey(ASN))
                        {
                            import_policies.put(ASN, new HashSet<>());
                            export_policies.put(ASN, new HashSet<>());
                        }

                        import_policies.get(ASN).addAll(autnum.imports);
                        export_policies.get(ASN).addAll(autnum.exports);

                        this.autnums.put(ASN, autnum);
                        break;
                    }
                    case ROUTE:
                    {
                        RpslAttribute origin = object.findAttribute(AttributeType.ORIGIN);
                        Long number = Long.parseLong(origin.getCleanValue().toUpperCase().replaceFirst("AS", ""));
                        String prefix = object.findAttribute(AttributeType.ROUTE).getCleanValue().toString();
                        ROUTE route = new ROUTE(prefix, number);

                        if (!asn_route.containsKey(number))
                            asn_route.put(number, new HashSet<>());
                        asn_route.get(number).add(route);

                        // Add the ROUTE to the tree.
                        addRoute(route);

                        for (RpslAttribute member : object.findAttributes(AttributeType.MEMBER_OF))
                            for (CIString value : member.getCleanValues())
                            {
                                if (!member_route.containsKey(value.toUpperCase()))
                                    member_route.put(value.toUpperCase(), new HashSet<>());

                                member_route.get(value.toUpperCase()).add(route);
                            }

                        break;
                    }
                    case ROUTE6:
                        //route6.add(object);
                        break;

                    case AS_BLOCK:
                        //as_block.add(object);
                        break;
                    case INETNUM:
                        //irr.inetnums.add(object);
                        break;
                    case INET6NUM:
                        //inetnum6.add(object);
                        break;
                    case RTR_SET:
                        break;
                    case MNTNER:
                        break;
                    case ROLE:
                        break;
                    case IRT:
                        break;
                    case DOMAIN:
                        break;
                    case POEM:
                        break;
                    case PERSON:
                        break;
                    case INET_RTR:
                        break;
                    case KEY_CERT:
                        break;
                    case POETIC_FORM:
                        break;
                    case ORGANISATION:
                        break;
                    default:
                        System.out.println("Not implemented: " + object.getType().toString());
                        break;
                }
            }
            catch(Exception e)
            {
                // Currently do nothing
            }
        }
    }

    /**
     * Adds a ROUTE object to the RPSL Route Tree.
     * @param route The ROUTE object that should be added to the Route Tree.
     */
    private void addRoute(ROUTE route)
    {
        try
        {
            IPAddress address = new IPAddressString(route.prefix).toAddress();
            BitSet set = BitSet.valueOf(address.getBytes());

            Node<Set<String>> node = routes.getExact(set, address.getPrefixLength());
            if(node == null || node.value == null)
            {
                Set<String> origins = new HashSet<>();
                origins.add("AS" + route.origin);
                routes.insert(set, address.getPrefixLength(), origins);
            }
            else
                node.value.add("AS" + route.origin);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public Set<ROUTE> getRoutes(Long ASN)
    {
        return asn_route.getOrDefault(ASN, new HashSet<>());
    }

    public AS_SET getAS_SET(String set)
    {
        if(!as_set.containsKey(set))
            as_set.put(set, new AS_SET(set, this));

        return as_set.get(set);
    }

    public FILTER_SET getFILTER_SET(String set)
    {
        if(!filter_set.containsKey(set))
            filter_set.put(set, new FILTER_SET(set, this));

        return filter_set.get(set);
    }

    public ROUTE_SET getROUTE_SET(String set)
    {
        if(!route_set.containsKey(set))
            route_set.put(set, new ROUTE_SET(set, this));

        return route_set.get(set);
    }

    public Set<RPSLPolicy> getImportPolicies(Long ASN)
    {
        return import_policies.getOrDefault(ASN, new HashSet<>());
    }

    public Set<RPSLPolicy> getExportPolicies(Long ASN)
    {
        return export_policies.getOrDefault(ASN, new HashSet<>());
    }
}
