package rpsl.entity;

public class ROUTE
{
    public final String prefix;
    public final Long origin;

    public ROUTE(String prefix, Long origin)
    {
        this.prefix = prefix;
        this.origin = origin;
    }
}
