package rpsl.expression.filter;

import irr.IRR;

import java.util.Queue;

import static rpsl.expression.filter.FilterMatch.ACCEPT;
import static rpsl.expression.filter.FilterMatch.REJECT;

/**
 * Expression: filter_factor :  OP_NOT filter_factor | '(' filter ')' | filter_operand
 */
public class FilterFactor implements Filterable
{
    private final Filterable filterable;

    private FilterFactor(Filterable filterable)
    {
        this.filterable = filterable;
    }

    /**
     *
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return True if the prefix is been accepted.
     */
    @Override
    public FilterMatch accept(String prefix, String peer)
    {
        if(filterable.accept(prefix, peer).equals(REJECT))
            return ACCEPT;
        else
            return REJECT;
    }

    public static Filterable create(Queue<String> queue, IRR registry) throws Exception
    {
        switch(queue.peek())
        {
            case "NOT":
            {
                queue.remove();

                Filterable f = FilterFactor.create(queue, registry);
                return new FilterFactor(f);
            }
            case "(":
            {
                queue.remove();
                Filterable f = Filter.create(queue, registry);
                if(queue.remove().equals(")"))
                    return f;
                else
                    throw new Exception("Missing closing parenthesis.");
            }
            default:
            {
                return FilterOperand.create(queue, registry);
            }
        }
    }
}
