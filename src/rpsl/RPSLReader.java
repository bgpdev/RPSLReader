package rpsl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * The RPSLReader reads RPSL formatted outputs according to RFC2622, RFC2650 and RFC4012
 */
public class RPSLReader
{
    public enum Mode
    {
        RIPE181,
        RPSL,
        RPSLng
    }

    /** The BufferedReader from which we will construct RPSLObjects. */
    private final BufferedReader reader;

    /** If an error is occurs, try to continue. */
    public boolean STRICT_PARSING = true;

    /** The mode which should be used to parse RPSL, either RIPE181, RPSL or RPSLng. */
    public Mode mode = Mode.RPSL;


    /**
     * Construct an RPSLReader from an InputStream.
     * @param stream The InputStream from which we will read RPSLObjects.
     */
    public RPSLReader(InputStream stream)
    {
        this.reader = new BufferedReader(new InputStreamReader(stream));
    }

    /**
     * Read one RPSLObject from the InputStream.
     * @return The next RPSLObject in the InputStream.
     * @throws Exception Exceptions that might occur during parsing.
     */
    public RPSLObject read() throws Exception
    {
        // The last property that has been accessed, used for multiline strings.
        String property = null;
        RPSLObject object = new RPSLObject();

        ArrayList<String> debug = new ArrayList<>();

        String line = reader.readLine();
        while(line != null)
        {
            debug.add(line);

            // If the line is empty, continue;
            if(line.length() == 0)
            {
                if(object.size() != 0)
                    return object;

                line = reader.readLine();
                continue;
            }

            switch (line.charAt(0))
            {
                // Comment (% is RIPE and RADB style, # is RPSL style)
                case '%':
                case '#':
                    break;

                // Continuation of the last property.
                case ' ':
                case '\t':
                case '+':
                    String x = object.getProperty(property) + "\n" + line.replaceFirst("\\+", "").trim();
                    if(property != null)
                        object.addProperty(property, x);
                    break;

                default:
                    String[] array = line.split(":", 2);

                    if (array.length == 1)
                    {
                        if(STRICT_PARSING)
                            throw new Exception("Line does not contain a colon separator. " + line);
                        else
                            break;
                    }

                    if(!Character.isAlphabetic(array[0].charAt(0)))
                    {
                        if (STRICT_PARSING)
                            throw new Exception("Property not starting with alphanumeric character. " + line);
                        else
                            break;
                    }

                    /*
                     * In case multiple of the same properties are present.
                     * TODO: Is this even possible in official RPSL
                     **/
                    if (object.hasProperty(array[0]))
                    {
                        String value = object.getProperty(array[0]) + "\n" + array[1].trim();
                        object.addProperty(array[0], value);
                    }
                    else
                        object.addProperty(array[0], array[1].trim());

                    property = array[0];
                    break;
            }

            // Read a new line.
            line = reader.readLine();
        }

        if(object.size() == 0)
            return null;
        else
            return object;
    }

    /**
     * Read all RPSLObjects from the InputStream.
     * @return An ArrayList of RPSLObjects read from the InputStream.
     * @throws Exception Exceptions that might occur during parsing.
     */
    public ArrayList<RPSLObject> readAll() throws Exception
    {
        // Construct ArrayList of RPSLObjects.
        ArrayList<RPSLObject> result = new ArrayList<>();

        // Read all the RPSLObjects,
        while(true)
        {
            RPSLObject object = read();
            if(object == null)
                break;
            else
                result.add(object);
        }

        return result;
    }

    /**
     * Closes the InputStream associated with the Reader
     * @throws IOException Any excepts that occur while closing
     */
    public void close() throws IOException
    {
        reader.close();
    }
}
