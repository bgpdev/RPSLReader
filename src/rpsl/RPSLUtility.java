package rpsl;

import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import irr.Settings;
import rpsl.expression.filter.FilterMatch;

public class RPSLUtility
{
    public static FilterMatch matches(String prefix, String prefix_op)
    {
        try
        {
            // Split on operator, if one is present.
            String[] split = prefix_op.split("\\^");

            IPAddress address1 = new IPAddressString(prefix).toAddress();
            IPAddress address2 = new IPAddressString(split[0]).toAddress();

            if(address1.equals(address2))
                return FilterMatch.EXACT_PREFIX_ACCEPT;

            if(!address2.contains(address1))
                return FilterMatch.REJECT;
            else if(Settings.RPSLPOLICY_ACCEPT_SPECIFIC_PREFIXES)
                return FilterMatch.SPECIFIC_PREFIX_ACCEPT;

            /* ---------------------------------
             * Check the operators manually.
             * ---------------------------------*/
            switch (split[1])
            {
                case "-":
                    if (address1.getPrefixLength() < address2.getPrefixLength())
                        return FilterMatch.EXACT_PREFIX_ACCEPT;
                case "+":
                    if (address1.getPrefixLength() <= address2.getPrefixLength())
                        return FilterMatch.EXACT_PREFIX_ACCEPT;
                default:
                    if(!split[0].contains("-"))
                        if (address1.getPrefixLength() == Integer.parseInt(split[0]))
                            return FilterMatch.EXACT_PREFIX_ACCEPT;

                    String[] numbers = split[0].split("-");
                    if(Integer.parseInt(numbers[0]) <= address1.getPrefixLength() &&
                            address1.getPrefixLength() <= Integer.parseInt(numbers[1]))
                        return FilterMatch.EXACT_PREFIX_ACCEPT;
            }
        }
        catch(Exception e)
        {
            System.out.println("Input: " + prefix + ", " + prefix_op);
            e.printStackTrace();
            System.exit(-1);
        }

        return FilterMatch.REJECT;
    }

    public static boolean isAS(String token)
    {
        return token.matches("AS[0-9]+");
    }

    public static boolean isAS_SET(String token)
    {
        return token.matches("AS-?[a-zA-Z0-9\\-_]+[:[a-zA-Z0-9\\-_]+]*");
    }

    public static boolean isROUTE_SET(String token)
    {
        return token.matches("(RS-[a-zA-Z0-9\\-:_]+|.*:RS-[a-zA-Z0-9\\-_]+)");
    }
}
