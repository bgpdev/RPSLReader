package rpsl.expression.filter;

import network.entities.Prefix;
import rpsl.RPSLUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class FilterPrefixList implements RangeOperatable
{
    private final List<String> prefixes = new ArrayList<>();

    /**
     *
     * @param prefix The prefix or ASN that should be matched against this object.
     * @return True if the prefix is accepted by this policy.
     */
    @Override
    public FilterMatch accept(String prefix, String peer)
    {
        for(String x : prefixes)
        {
            // TODO: Should be fixed.
            if(x.equals("0.0.0.0/0") && !x.equals(prefix))
                return FilterMatch.REJECT;

            FilterMatch match = RPSLUtility.matches(prefix, x);
            if (!match.equals(FilterMatch.REJECT))
                return match;
        }

        return FilterMatch.REJECT;
    }

    @Override
    public FilterMatch accept(String prefix, String peer, String operator)
    {
        // TODO: Currently if the specific value is filtering more then the outer operator it fails.
        for(String x : prefixes)
        {
            FilterMatch match = RPSLUtility.matches(prefix, x.split("\\^")[0] + "^" + operator);
            if (!match.equals(FilterMatch.REJECT))
                return match;
        }

        return FilterMatch.REJECT;
    }

    public static RangeOperatable create(Queue<String> queue) throws Exception
    {
        if(!queue.remove().equals("{"))
            throw new Exception("Missing opening bracket parenthesis");

        FilterPrefixList list = new FilterPrefixList();
        while(!queue.peek().equals("}"))
        {
            String token = queue.remove();
            if(Prefix.isPrefix(token))
            {
                if(queue.peek().equals("^"))
                    token += queue.remove() + queue.remove();
                list.prefixes.add(token);
            }
            else
                throw new Exception("Invalid Token: " + token);

            if(!queue.peek().equals("}") && !queue.remove().equals(","))
                throw new Exception("Missing comma: " + token);
        }

        // Remove the closing '}' from the queue.
        queue.remove();
        return list;
    }
}
