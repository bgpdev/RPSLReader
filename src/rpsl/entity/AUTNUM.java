package rpsl.entity;

import irr.IRR;
import net.ripe.db.whois.common.rpsl.AttributeType;
import net.ripe.db.whois.common.rpsl.RpslAttribute;
import net.ripe.db.whois.common.rpsl.RpslObject;
import rpsl.RPSLPolicy;
import rpsl.RPSLEntity;
import rpsl.RPSLTokenizer;
import rpsl.expression.filter.FilterOperand;
import rpsl.expression.filter.FilterPrefixOperand;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class AUTNUM extends RPSLEntity
{
    public final Long ASN;
    public final ArrayList<RPSLPolicy> imports = new ArrayList<>();
    public final ArrayList<RPSLPolicy> exports = new ArrayList<>();

    public AUTNUM(RpslObject object, IRR registry)
    {
        ASN = Long.parseLong(object.findAttribute(AttributeType.AUT_NUM).getCleanValue().toString().toUpperCase().replaceAll("AS", ""));

        if(object.containsAttribute(AttributeType.IMPORT))
        {
            for(RpslAttribute a : object.findAttributes(AttributeType.IMPORT))
            {
                try
                {
                    imports.add(new RPSLPolicy(ASN, a, registry));
                }
                catch(Exception e)
                {
                    if(!e.getMessage().equals("NO MATCHING TOKEN: COMMUNITY.CONTAINS"))
                    {

                        try
                        {
                            Thread.sleep(50);
                        } catch (InterruptedException e1)
                        {
                            e1.printStackTrace();
                        }

                        System.out.println("--------------------------------");
                        System.out.println("ASN" + ASN);
                        System.out.println(RPSLTokenizer.tokenize(a.getValue()));
                        System.out.println("Message: " + e.getMessage());
                        System.out.println("--------------------------------");
                    }
                }
            }
        }

        if(object.containsAttribute(AttributeType.EXPORT))
        {
            for(RpslAttribute a : object.findAttributes(AttributeType.EXPORT))
                try
                {
                    exports.add(new RPSLPolicy(ASN, a, registry));
                }
                catch(Exception e)
                {
                    if(!e.getMessage().equals("NO MATCHING TOKEN: COMMUNITY.CONTAINS"))
                    {

                        try
                        {
                            Thread.sleep(50);
                        } catch (InterruptedException e1)
                        {
                            e1.printStackTrace();
                        }

                        System.out.println("--------------------------------");
                        System.out.println("ASN" + ASN);
                        System.out.println(RPSLTokenizer.tokenize(a.getValue()));
                        System.out.println("Message: " + e.getMessage());
                        System.out.println("--------------------------------");
                    }
                }
        }

//        if(object.containsAttribute(AttributeType.MP_IMPORT))
//        {
//            for(RpslAttribute a : object.findAttributes(AttributeType.MP_IMPORT))
//                imports.add(new RPSLPolicy(ASN, a.getTokenList(), registry));
//        }
//
//        if(object.containsAttribute(AttributeType.MP_EXPORT))
//        {
//            for(RpslAttribute a : object.findAttributes(AttributeType.MP_EXPORT))
//                exports.add(new RPSLPolicy(ASN, a.getTokenList(), registry));
//        }
    }
}
