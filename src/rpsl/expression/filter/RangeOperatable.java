package rpsl.expression.filter;

public interface RangeOperatable extends Filterable
{
    FilterMatch accept(String prefix, String peer, String operator);
}
